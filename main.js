let counter = 0;

function addToCounter(a) {
  counter += a;
  updateView();
}

function updateView() {
  document.querySelector("#root").innerText = `${counter}`;
}
