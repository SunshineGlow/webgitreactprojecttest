# WebGitReactProjectTest

На данный момент выполнено: 
1. Создан репозиторий на GitLab
2. Изучены основы Git
3. Создан index.html с "Hello, World!"
4. Применены стили к заголовкам
5. Изменен DOM с помощью JS
6. Использованы переменные
7. Добавлена функция вычисления факториала
8. Добавлен счетчик
